
function merge(arr,l,r,m){
    let leftSize = m-l+1;
    let rightSize = r-m;
    let leftArr =[];
    let rightArr =[];
    for(let i=0; i<leftSize; i++){
        leftArr.push(arr[i+l]);
    }
    for(let i=0; i<rightSize; i++){
        rightArr.push(arr[i+m+1]);
    }
    let k =l, i=0, j=0;
    while(i < leftSize && j < rightSize){
        if(leftArr[i] < rightArr[j]){
            arr[k] = leftArr[i];
            i++;
        }
        else{
            arr[k] = rightArr[j];
            j++;
        }
        k++;
    }
    while(i<leftSize){
        arr[k] = leftArr[i];
        i++;
        k++;
    }
    while(j<rightSize){
        arr[k] = rightArr[j];
        j++;
        k++;
    }

}

function mergeSort(arr,l,r){
    if(l>=r)
        return;
    let m = l + Math.trunc((r-l)/2);
    mergeSort(arr,l,m);
    mergeSort(arr,m+1,r);
    merge(arr,l,r,m);
}

a = [12,99,2,0,33,11,5,2,8,23];

mergeSort(a,0,a.length-1);
console.log(a);
